package ar.com.rockcodes.rockautomensaje;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Msg {

	private static String tag = ChatColor.GOLD+"["+ChatColor.DARK_AQUA+"AutoMsg"+ChatColor.GOLD+"]"+ChatColor.RESET+"- ";
	
	public static void sendMsg(Player player, String str){
		player.sendMessage(tag+str);
	}

	public static void sendMsg(CommandSender sender, String str) {
		sender.sendMessage(tag+str);
		
	}
	
}
