package ar.com.rockcodes.rockautomensaje;

import org.bukkit.plugin.java.JavaPlugin;


public class AutoMensaje extends JavaPlugin {

	public static AutoMensaje plugin ;
	
	@Override
	public void onDisable() {
		Settings.saveSettings();
	}

	@Override
	public void onEnable() {
		AutoMensaje.plugin = this;
		// TODO Auto-generated method stub
		this.saveDefaultConfig();
		Settings.loadSettings();
		getCommand("automsg").setExecutor(new Comandos());
		long intervalo = Long.valueOf((Settings.intervalo*60)*20);
	    getServer().getScheduler().scheduleSyncRepeatingTask(this, new MensajeTask(), 1200L, intervalo);
	       
	}
	
	

	
	
}
