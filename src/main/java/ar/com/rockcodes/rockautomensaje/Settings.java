package ar.com.rockcodes.rockautomensaje;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;



public class Settings {

	
	public static Boolean debug = false;
	public static Boolean enable = true;
	public static List<String> mensajes = new ArrayList<String>();
	public static int intervalo = 10;
	public static boolean displayonbar = true;
	public static String tag = "[AutoMsg]";
	
	public static void loadSettings(){
		
		FileConfiguration config = AutoMensaje.plugin.getConfig() ;
	
		
		//GENERAL CONFIG
		
		Settings.debug = config.getBoolean("debug",true);
		Settings.enable = config.getBoolean("enable",true);
		Settings.mensajes = (List<String>) config.getList("mensajes",new ArrayList<String>());
		Settings.tag = config.getString("tag","[AutoMsg]");
		Settings.intervalo = config.getInt("intervalo",10);
		Settings.displayonbar = config.getBoolean("displayonbar",true);
		//MYSQL CONFIG
		
		
	}
	
	public static void reload(){
		AutoMensaje.plugin.reloadConfig();
		Settings.loadSettings();
	}
	
	public static void saveSettings(){
		FileConfiguration config = AutoMensaje.plugin.getConfig() ;
		config.set("debug", Settings.debug);
		config.set("enable", Settings.enable);
		config.set("mensajes", Settings.mensajes);
		config.set("intervalo", Settings.intervalo);
		config.set("tag", Settings.tag);
		config.set("displayonbar", Settings.displayonbar);
		AutoMensaje.plugin.saveConfig();
	}
	
	
}
