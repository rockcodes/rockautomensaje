package ar.com.rockcodes.rockautomensaje;

import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Comandos implements CommandExecutor{

	
	@Override
	public boolean onCommand(CommandSender sender, Command command,String label, String[] args) {
		
		
		
		if (sender instanceof Player) {
			Player player = (Player)sender;
			if(!player.isOp()) {
				Msg.sendMsg(player, "Only OP can acces this command.");
				return true;
			}
		}
		if(args.length==0){command_help(sender,args); return true;}
			
		switch(args[0]){
			case "reload": command_reload(sender,args); return true;
			case "enable": command_enable(sender,args); return true;
			case "add": command_addmsg(sender,args); return true;
			case "del": command_delmsg(sender,args); return true;
			case "list": command_listmsg(sender,args); return true;
			case "help": command_help(sender,args); return true;	
		}
			
		
		return true;
	}

	private void command_enable(CommandSender sender, String[] args) {
		if(args.length<2){
	
			Msg.sendMsg(sender,"Please use /automsg enable <true/false> ");
			return;
		}
		
		boolean val = Boolean.valueOf(args[1]);
		Settings.enable = val;
		Settings.saveSettings();
		Msg.sendMsg(sender, "Enable set = "+args[1]);
	}

	private void command_listmsg(CommandSender sender, String[] args) {
		
		int i = 0;
		for(String msg :Settings.mensajes){
			
			String mensaje = "["+i+"]";
			
			if(msg.length()>15)
				mensaje += msg.substring(0, 15)+"...";
			else 
				mensaje += msg;
				
			Msg.sendMsg(sender,mensaje);
			i++;
		}
		
	}

	private void command_delmsg(CommandSender sender, String[] args) {
		if(args.length<2){
			Msg.sendMsg(sender,"Please use /automsg del <id> ");
			return;
		}
		
		int id = Integer.valueOf(args[1]);
		Settings.mensajes.remove(id);
		sender.sendMessage("Msg removed.");
		Settings.saveSettings();
	}

	private void command_addmsg(CommandSender sender, String[] args) {
		if(args.length<2){
			Msg.sendMsg(sender,"Argumentos insuficientes /automsg add <message> ");
			return;
		}
		
		String msg = "";
		for(int i = 1; i<args.length;i++){
			msg +=" "+args[i];
		}
		
		Settings.mensajes.add(msg);
		Msg.sendMsg(sender,"Msg added.");
		Settings.saveSettings();
	}

	private void command_reload(CommandSender sender, String[] args) {
		Settings.reload();
		Msg.sendMsg(sender,"Config reloaded!.");
	}

	private void command_help(CommandSender sender, String[] args) {
		Msg.sendMsg(sender,"Commands:");
		sender.sendMessage("/automsg add <msg> - Add message to the list.");
		sender.sendMessage("/automsg del <id> - Remove message width <id> ");
		sender.sendMessage("/automsg list - Show all messages with their ids");
		sender.sendMessage("/automsg reload - Reload config");
		sender.sendMessage("/automsg help - Show help");
	}
}
