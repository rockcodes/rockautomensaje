package ar.com.rockcodes.rockautomensaje;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import ar.com.rockcodes.rockautomensaje.util.ActionBar;

public class MensajeTask implements Runnable {

	int msgid = 0;
	
	public MensajeTask (){
	}
	
	@Override
	public void run() {
		
		if(!Settings.enable) return;
		if(Settings.mensajes.size()>0){
			
			if(Settings.displayonbar){
				for ( Player p : AutoMensaje.plugin.getServer().getOnlinePlayers()){
					ActionBar.send(p, this.colorizeText(Settings.tag+Settings.mensajes.get(msgid)));
				}
			}else{
				
				Bukkit.getServer().broadcastMessage(this.colorizeText(Settings.tag+Settings.mensajes.get(msgid)));
			}
			
			this.msgid ++;
			if(msgid>=Settings.mensajes.size())this.msgid = 0;
		}
		

	}

	public String colorizeText(String string) {
	    string = string.replaceAll("&0", ChatColor.BLACK+"");
	    string = string.replaceAll("&1", ChatColor.DARK_BLUE+"");
	    string = string.replaceAll("&2", ChatColor.DARK_GREEN+"");
	    string = string.replaceAll("&3", ChatColor.DARK_AQUA+"");
	    string = string.replaceAll("&4", ChatColor.DARK_RED+"");
	    string = string.replaceAll("&5", ChatColor.DARK_PURPLE+"");
	    string = string.replaceAll("&6", ChatColor.GOLD+"");
	    string = string.replaceAll("&7", ChatColor.GRAY+"");
	    string = string.replaceAll("&8", ChatColor.DARK_GRAY+"");
	    string = string.replaceAll("&9", ChatColor.BLUE+"");
	    string = string.replaceAll("&a", ChatColor.GREEN+"");
	    string = string.replaceAll("&b", ChatColor.AQUA+"");
	    string = string.replaceAll("&c", ChatColor.RED+"");
	    string = string.replaceAll("&d", ChatColor.LIGHT_PURPLE+"");
	    string = string.replaceAll("&e", ChatColor.YELLOW+"");
	    string = string.replaceAll("&f", ChatColor.WHITE+"");
	    return string;
	}


}
